//
//  Move+AdditionsTest.swift
//  Blinkist-SampleTests
//
//  Created by ___________ on 11/03/2020.
//  Copyright © 2020 idk. All rights reserved.
//

import XCTest
import ConnectFourGame
@testable import Blinkist_Sample

class Move_AdditionsTest: XCTestCase {

    func testMoveInitWithCoords() {
        // GIVEN
        let x = 0
        let y = 0
        let owner = 0
        let indexPath = IndexPath(item: 0, section: 0)
        let expectedMove = Move(x: x, y: y, owner: owner)

        // WHEN
        let actualMove = Move(owner: owner, indexPath: indexPath)

        // THEN
        XCTAssertEqual(actualMove.x, expectedMove.x)
        XCTAssertEqual(actualMove.y, expectedMove.y)
        XCTAssertEqual(actualMove.owner, expectedMove.owner)
    }
}
