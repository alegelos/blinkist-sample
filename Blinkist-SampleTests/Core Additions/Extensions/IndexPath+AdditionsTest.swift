//
//  IndexPath+AdditionsTest.swift
//  Blinkist-SampleTests
//
//  Created by ___________ on 11/03/2020.
//  Copyright © 2020 idk. All rights reserved.
//

import XCTest
@testable import Blinkist_Sample

class IndexPath_AdditionsTest: XCTestCase {

    func testInitIndexPathWithCoords() {
        // GIVEN
        let expectedIndexPath = IndexPath(item: 2, section: 0)
        let boardX = 2
        let boardY = 0

        // WHEN
        let actualIndexPath = IndexPath(boardX: boardX, boardY: boardY)

        // THEN
        XCTAssertEqual(actualIndexPath, expectedIndexPath)
    }

    func testConnectFourBoardCoordinates() {
        // GIVEN
        let indexPath = IndexPath(item: 2, section: 0)
        let expectedBoardX = 2
        let expectedBoardY = 0

        // WHEN
        let actualCoords = indexPath.connectFourBoardCoordinates()

        // THEN
        XCTAssertEqual(actualCoords.x, expectedBoardX)
        XCTAssertEqual(actualCoords.y, expectedBoardY)
    }
}
