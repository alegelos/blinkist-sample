//
//  ConnectFourBoard+AdditionsTest.swift
//  Blinkist-SampleTests
//
//  Created by ___________ on 11/03/2020.
//  Copyright © 2020 idk. All rights reserved.
//

import XCTest
import ConnectFourGame
@testable import Blinkist_Sample

class ConnectFourBoard_AdditionsTest: XCTestCase {

    var connectFour: ConnectFour?

    override func setUp() {
        connectFour = ConnectFour(connectTimes: 4,
                                  boardLenght: 7,
                                  boardHeight: 6)
    }

    override func tearDown() {
        connectFour = nil
    }

    func testOwner() {
        // GIVEN
        guard let connectFour = connectFour else {
            XCTFail()
            return
        }
        let expectedOwner = 1
        let indexPath = IndexPath(item: 0, section: 0)
        let move = Move(owner: expectedOwner, indexPath: indexPath)
        connectFour.doMove(move)


        // WHEN
        let actualOwner = connectFour.board.owner(indexPath)

        // THEN
        XCTAssertEqual(actualOwner, expectedOwner)
    }
}
