//
//  UIColor+AdditionsTest.swift
//  Blinkist-SampleTests
//
//  Created by ___________ on 11/03/2020.
//  Copyright © 2020 idk. All rights reserved.
//

import XCTest

class UIColor_AdditionsTest: XCTestCase {

    func testBlackColor() {
        // GIVEN
        let hexWhiteColor = "#000000"
        let expectedColor = UIColor(red: 0, green: 0, blue: 0, alpha: 1)
        // WHEN
        let actualColor = UIColor(hex: hexWhiteColor)

        // THEN
        XCTAssertEqual(actualColor, expectedColor)
    }

    func testWhiteColor() {
        // GIVEN
        let hexWhiteColor = "#FFFFFF"
        let expectedColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)
        // WHEN
        let actualColor = UIColor(hex: hexWhiteColor)

        // THEN
        XCTAssertEqual(actualColor, expectedColor)
    }
}
