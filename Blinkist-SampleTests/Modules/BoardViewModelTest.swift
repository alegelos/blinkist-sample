//
//  BoardViewModelTest.swift
//  Blinkist-SampleTests
//
//  Created by ___________ on 11/03/2020.
//  Copyright © 2020 idk. All rights reserved.
//

import XCTest
import Moya
import ConnectFourGame
@testable import Blinkist_Sample

class BoardViewModelTest: XCTestCase {

    // Class to test
    var boardViewModel: BoardViewModel?

    // Helpers
    var gameServices: MoyaProvider<GameServices>?
    var connectFour: ConnectFour?

    var actualStatus: BoardViewModelStatus?
    var expectedStatus: BoardViewModelStatus?

    let timeOut = TimeInterval(3)
    var playersConfig = [Player]()

    override func setUp() {
        let gameServices = MoyaProvider<GameServices>(stubClosure: MoyaProvider.immediatelyStub)
        let connectFour = ConnectFour(connectTimes: 4,
                                      boardLenght: 6,
                                      boardHeight: 7)
        self.connectFour = connectFour
        self.gameServices = gameServices
        actualStatus = BoardViewModelStatus(playerConfig: nil)
        boardViewModel = BoardViewModel(gameServices: gameServices,
                                        connectFour: connectFour,
                                        delegate: self)
    }

    override func tearDown() {
        boardViewModel = nil
        connectFour = nil
        gameServices = nil
        actualStatus = nil
        expectedStatus = nil
    }

    func testHandleDidPressWithEmptyBoard() {
        // GIVEN
        let moveIndexPath = IndexPath(item: 3, section: 0)
        guard let expectedIndexPath = connectFour?.columnFreeSlot(at: moveIndexPath) else {
            XCTFail()
            return
        }
        expectedStatus = BoardViewModelStatus(didCallReloadBoard: true,
                                              didCallReloadIndexs: [expectedIndexPath],
                                              didCallShowLoading: true,
                                              didCallHiveLoading: true)

        // WHEN
        boardViewModel?.handleDidPress(moveIndexPath)

        //THEN
        BoardViewModelTest.assetEquals(actualStatus, expectedStatus)
    }

    func testHandleDidPressWithWinningMove() {
        // GIVEN
        let move1 = Move(x: 0, y: 5, owner: 0)
        let move2 = Move(x: 1, y: 5, owner: 0)
        let move3 = Move(x: 2, y: 5, owner: 0)
        connectFour?.doMove(move1)
        connectFour?.doMove(move2)
        connectFour?.doMove(move3)

        let moveIndexPath = IndexPath(item: 3, section: 0)
        guard let expectedIndexPath = connectFour?.columnFreeSlot(at: moveIndexPath) else {
            XCTFail()
            return
        }
        expectedStatus = BoardViewModelStatus(didCallReloadBoard: true,
                                              didCallReloadIndexs: [expectedIndexPath],
                                              winnerPlayer: playersConfig[0],
                                              didCallShowLoading: true,
                                              didCallHiveLoading: true)

        // WHEN
        boardViewModel?.handleDidPress(moveIndexPath)

        //THEN
        BoardViewModelTest.assetEquals(actualStatus, expectedStatus)
    }
}

// MARK: - BoardViewModelDelegate
extension BoardViewModelTest: BoardViewModelDelegate {
    func setPlayersConfig(_ playersConfig: [Player]) {
        self.playersConfig = playersConfig
        actualStatus?.playerConfig = playersConfig
    }

    func reloadBoard(_ board: ConnectFour.ConnectFourBoard) {
        actualStatus?.didCallReloadBoard = true
    }

    func reloadBoardItems(_ board: ConnectFour.ConnectFourBoard, indexPaths: [IndexPath]) {
        actualStatus?.didCallReloadIndexs = indexPaths
    }

    func showAlert(message: BoardErrors.Alerts) {
        actualStatus?.alertMessage = message
    }

    func showWinner(_ player: Player) {
        actualStatus?.winnerPlayer = player
    }

    func showLoading() {
        actualStatus?.didCallShowLoading = true
    }

    func hideLoading() {
        actualStatus?.didCallHiveLoading = true
    }
}

// Helping Structures
extension BoardViewModelTest {
    private static func assetEquals(_ actual: BoardViewModelStatus?, _ expected: BoardViewModelStatus?) {
        guard let actualPlayerConfig = actual?.playerConfig,
            actual?.playerConfig?.count == expected?.playerConfig?.count else {
                XCTFail()
                return
        }
        for i in 0..<actualPlayerConfig.count {
            XCTAssertEqual(actual?.playerConfig?[i].color, expected?.playerConfig?[i].color)
            XCTAssertEqual(actual?.playerConfig?[i].name, expected?.playerConfig?[i].name)
        }
        XCTAssertEqual(actual?.didCallReloadBoard, expected?.didCallReloadBoard)
        XCTAssertEqual(actual?.didCallReloadIndexs, expected?.didCallReloadIndexs)
        XCTAssertEqual(actual?.alertMessage, expected?.alertMessage)
        XCTAssertEqual(actual?.winnerPlayer?.color, expected?.winnerPlayer?.color)
        XCTAssertEqual(actual?.winnerPlayer?.name, expected?.winnerPlayer?.name)
        XCTAssertEqual(actual?.didCallShowLoading, expected?.didCallShowLoading)
        XCTAssertEqual(actual?.didCallHiveLoading, expected?.didCallHiveLoading)

    }
    struct BoardViewModelStatus {
        var playerConfig: [Player]?
        var didCallReloadBoard: Bool
        var didCallReloadIndexs: [IndexPath]?
        var alertMessage: BoardErrors.Alerts?
        var winnerPlayer: Player?
        var didCallShowLoading: Bool
        var didCallHiveLoading: Bool

        init(playerConfig: [Player]? = [Player(name: "Sue", color: "#FF0000"), Player(name: "Henry", color: "#0000FF")],
             didCallReloadBoard: Bool = false,
             didCallReloadIndexs: [IndexPath]? = nil,
             alertMessage: BoardErrors.Alerts? = nil,
             winnerPlayer: Player? = nil,
             didCallShowLoading: Bool = false,
             didCallHiveLoading: Bool = false) {

            self.playerConfig = playerConfig
            self.didCallReloadBoard = didCallReloadBoard
            self.didCallReloadIndexs = didCallReloadIndexs
            self.alertMessage = alertMessage
            self.winnerPlayer = winnerPlayer
            self.didCallShowLoading = didCallShowLoading
            self.didCallHiveLoading = didCallHiveLoading
        }
    }
}
