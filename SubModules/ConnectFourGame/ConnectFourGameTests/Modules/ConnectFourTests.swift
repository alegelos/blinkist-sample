//
//  ConnectFourTests.swift
//  ConnectFourGameTests
//
//  Created by ___________ on 09/03/2020.
//  Copyright © 2020 idk. All rights reserved.
//

import XCTest
@testable import ConnectFourGame

class ConnectFourTests: XCTestCase {
    let connectTimes = 4
    let boardLenght = 7
    let boardHeight = 6

    var connectFour: ConnectFour?

    override func setUp() {
        connectFour = ConnectFour(connectTimes: connectTimes,
                                  boardLenght: boardLenght,
                                  boardHeight: boardHeight)
    }

    override func tearDown() {
        connectFour = nil
        
    }

    func testStartGameWithEmptyBoard() {
        // GIVEN
        // WHEN
        guard let connectFour = connectFour else {
            XCTFail()
            return
        }

        // THEN
        XCTAssertTrue(isBoardEmpty(connectFour.board))
    }

    func testBoardSize() {
        // GIVEN
        // WHEN
        guard let connectFour = connectFour else {
            XCTFail()
            return
        }

        // THEN
        XCTAssertEqual(connectFour.board.count, boardLenght)
        for y in connectFour.board {
            XCTAssertEqual(y.count, boardHeight)
        }
    }

    func testDoRandomMoves() {
        // GIVEN
        let moves = fillBoardMoves(owner: Int.random(in: 0..<2))

        // WHEN
        moves.forEach { connectFour?.doMove($0) }

        // THEN
        moves.forEach { XCTAssertEqual(connectFour?.board[$0.x][$0.y], $0.owner) }
    }

    func testIsWinnerMoveWithAllMoves() {
        // GIVEN
        let moves = fillBoardMoves(owner: 1)

        // WHEN
        moves.forEach { connectFour?.doMove($0) }

        // THEN
        moves.forEach { XCTAssertTrue(connectFour?.isWinnerMove($0) ?? false) }
    }

    func testIsWinnerMoveWithAllMovesFromOtherPlayer() {
        // GIVEN
        let playerOneMoves = fillBoardMoves(owner: 1)
        let playerTwoMoves = fillBoardMoves(owner: 0)

        // WHEN
        playerOneMoves.forEach { connectFour?.doMove($0) }

        // THEN
        playerTwoMoves.forEach { XCTAssertFalse(connectFour?.isWinnerMove($0) ?? true) }
    }


    func testIsWinnerMoveWithNoneMoves() {
        // GIVEN
        let moves = fillBoardMoves(owner: 1)

        // WHEN
        // THEN
        moves.forEach { XCTAssertFalse(connectFour?.isWinnerMove($0) ?? true) }
    }

    func testIsHorizontalWinnerMove() {
        // GIVEN
        let moves = horizontalMoves(owner: 1)

        // WHEN
        moves.forEach { connectFour?.doMove($0) }

        // THEN
        moves.forEach { XCTAssertTrue(connectFour?.isWinnerMove($0) ?? false) }
    }

    func testIsVerticalWinnerMove() {
        // GIVEN
        let moves = verticalsMoves(owner: 1)

        // WHEN
        moves.forEach { connectFour?.doMove($0) }

        // THEN
        moves.forEach { XCTAssertTrue(connectFour?.isWinnerMove($0) ?? false) }
    }

    func testIsAscendingDiagonallWinnerMove() {
        // GIVEN
        let moves = ascendingDiagonallMoves(owner: 1)

        // WHEN
        moves.forEach { connectFour?.doMove($0) }

        // THEN
        moves.forEach { XCTAssertTrue(connectFour?.isWinnerMove($0) ?? false) }
    }

    func testIsDescendingDiagonalWinnerMove() {
        // GIVEN
        let moves = descendingDiagonalMoves(owner: 1)

        // WHEN
        moves.forEach { connectFour?.doMove($0) }

        // THEN
        moves.forEach { XCTAssertTrue(connectFour?.isWinnerMove($0) ?? false) }
    }
}

// MARK: - Private
extension ConnectFourTests {
    private func isBoardEmpty(_ board: ConnectFour.ConnectFourBoard) -> Bool {
        for y in board {
            for owner in y {
                guard owner == nil else {
                    return false
                }
            }
        }
        return true
    }

    private func fillBoardMoves(owner: Int) -> [Move]{
        var moves = [Move]()
        for x in 0..<boardLenght {
            for y in 0..<boardHeight {
                moves.append(Move(x: x, y: y, owner: owner))
            }
        }
        return moves
    }

    private func horizontalMoves(owner: Int) -> [Move] {
        var moves = [Move]()
        let y = Int.random(in: 0..<boardHeight)
        for x in 0..<boardLenght {
            moves.append(Move(x: x, y: y, owner: owner))
        }
        return moves
    }

    private func verticalsMoves(owner: Int) -> [Move] {
        var moves = [Move]()
        let x = Int.random(in: 0..<boardLenght)
        for y in 0..<boardHeight {
            moves.append(Move(x: x, y: y, owner: owner))
        }
        return moves
    }

    private func ascendingDiagonallMoves(owner: Int) -> [Move] {
        var moves = [Move]()
        for x in 0..<boardLenght {
            for y in 0..<boardHeight {
                guard x + y == boardLenght else { continue }
                moves.append(Move(x: x, y: y, owner: owner))
            }
        }
        return moves
    }

    private func descendingDiagonalMoves(owner: Int) -> [Move] {
        var moves = [Move]()
        for x in 0..<boardLenght {
            for y in 0..<boardHeight {
                guard x == y else { continue }
                moves.append(Move(x: x, y: y, owner: owner))
            }
        }
        return moves
    }
}
