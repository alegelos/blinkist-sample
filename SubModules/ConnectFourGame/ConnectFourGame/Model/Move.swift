//
//  Move.swift
//  ConnectFourGame
//
//  Created by ___________ on 09/03/2020.
//  Copyright © 2020 idk. All rights reserved.
//

import Foundation

public struct Move {
    public let x: Int
    public let y: Int
    public let owner: Int

    public init(x: Int, y: Int, owner: Int) {
        self.x = x
        self.y = y
        self.owner = owner
    }
}
