//
//  Player.swift
//  ConnectFourGame
//
//  Created by ___________ on 09/03/2020.
//  Copyright © 2020 idk. All rights reserved.
//

import Foundation

public struct Player {
    public let name: String
    public let color: String

    public init(name: String, color: String) {
        self.name = name
        self.color = color
    }
}
