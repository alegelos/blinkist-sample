//
//  ConnectFour.swift
//  ConnectFourGame
//
//  Created by ___________ on 09/03/2020.
//  Copyright © 2020 idk. All rights reserved.
//

import Foundation

public protocol ConnectFourDelegate: class {
    func didChangeBoard()
    func didChange(boardItems: [(x: Int, y: Int)])
}

/// Simulate a Connect Four board game.
///
/// Call doMove(:_) to add a new move to the board
/// Call isWinnerMove(:_) to check is we got a winner
public class ConnectFour {
    public typealias ConnectFourBoard = ContiguousArray<ContiguousArray<Int?>>
    private let connectTimes: Int
    private let maxX: Int
    private let maxY: Int
    private var _board: ConnectFourBoard

    public weak var delegate: ConnectFourDelegate?

    /// Game board.
    /// The first array is the X axis and the second array is the Y axis.
    public var board: ConnectFourBoard { _board }

    /// Start a new game with empty board.
    public init(connectTimes: Int,
                boardLenght: Int,
                boardHeight: Int,
                delegate: ConnectFourDelegate? = nil) {
        maxX = boardLenght
        maxY = boardHeight
        self.connectTimes = connectTimes
        self.delegate = delegate
        _board = ConnectFour.initBoard(maxX: maxX, maxY: maxY)
        delegate?.didChangeBoard()
    }

    /// Add a new move to the board.
    /// - Parameter newMove: move to add to the board
    public func doMove(_ newMove: Move) {
        let newOwner = newMove.owner
        let x = newMove.x
        let y = newMove.y

        _board[x][y] = newOwner
        delegate?.didChange(boardItems: [(x: x, y: y)])
    }

    /// Check if last move was a winning move.
    /// - Parameters:
    ///   - newMove: last move
    ///   - board: all board cells with board chipss owners
    public func isWinnerMove(_ newMove: Move) -> Bool {
        let times = connectTimes - 1

        if searchWinnerHorizontally(times: times, newMove: newMove) {
            return true
        }

        if searchWinnerVertically(times: times, newMove: newMove) {
            return true
        }

        if searchWinnerAscendingDiagonal(times: times, newMove: newMove) {
            return true
        }

        if searchWinnerDescendingDiagonal(times: times, newMove: newMove) {
            return true
        }

        return false
    }
}

// MARK: - Private
extension ConnectFour {
    private enum Direction {
        case up, down, right, left, upLeft, upRight, downLeft, downRight
    }

    /// Return the initial state of the board, with no chip. Nil means there's no chip.
    ///
    /// Note: Use the Int to represent the owner of the chip.
    private static func initBoard(maxX: Int, maxY: Int) -> ConnectFourBoard {
        ConnectFourBoard(repeating: ContiguousArray(repeating: nil, count: maxY), count: maxX)
    }

    /// Count the number of a player chips in a direcction
    /// - Parameters:
    ///   - direction: direction to count
    ///   - lineLength: how many chips should continue counting on this line
    ///   - move: current move
    ///   - board: all board cells with board chipss owner
    private func inLineChips(_ direction: Direction,
                                    lineLength: Int,
                                    move: Move) -> Int {

        let nextCoords = nextCoordinates(move, in: direction)

        // Stop searching, lineLength less than one
        guard lineLength > 0 else {
            return 0
        }

        // Stop if out of bounds
        guard nextCoords.x < maxX, nextCoords.x >= 0,
            nextCoords.y < maxY, nextCoords.y >= 0 else {
                return 0
        }

        // Stop if next space is NOT mine
        guard let owner = _board[nextCoords.x][nextCoords.y],
            owner == move.owner else {
                return 0
        }

        // Stop at lineLength equals to one
        guard lineLength > 1 else {
            return 1
        }

        let newTimes = lineLength - 1
        let newMove = Move(x: nextCoords.x, y: nextCoords.y, owner: move.owner)
        return 1 + inLineChips(direction, lineLength: newTimes, move: newMove)
    }

    private func nextCoordinates(_ move: Move, in direction: Direction) -> Move {
        var x = move.x
        var y = move.y

        switch direction {
        case .down:
            y = y + 1
        case .downLeft:
            y = y + 1
            x = x - 1
        case .downRight:
            y = y + 1
            x = x + 1
        case .up:
            y = y - 1
        case .upLeft:
            y = y - 1
            x = x - 1
        case .upRight:
            y = y - 1
            x = x + 1
        case .right:
            x = x + 1
        case .left:
            x = x - 1
        }

        return Move(x: x, y: y, owner: move.owner)
    }

    private func searchWinnerHorizontally(times: Int, newMove: Move) -> Bool{
        let leftChips = inLineChips(.left, lineLength: times, move: newMove)
        if leftChips >= times { return true }

        let rightChips = inLineChips(.right, lineLength: times - leftChips, move: newMove)
        if leftChips + rightChips >= times { return true }

        return false
    }

    private func searchWinnerVertically(times: Int, newMove: Move) -> Bool{
        let upChips = inLineChips(.up, lineLength: times, move: newMove)
        if upChips >= times { return true }

        let downChips = inLineChips(.down, lineLength: times - upChips, move: newMove)
        if upChips + downChips >= times { return true }

        return false
    }

    private func searchWinnerAscendingDiagonal(times: Int, newMove: Move) -> Bool{
        let upRightChips = inLineChips(.upRight, lineLength: times, move: newMove)
        if upRightChips >= times { return true }

        let downLeftChips = inLineChips(.downLeft, lineLength: times - upRightChips, move: newMove)
        if upRightChips + downLeftChips >= times { return true }

        return false
    }

    private func searchWinnerDescendingDiagonal(times: Int, newMove: Move) -> Bool{
        let upLeftChips = inLineChips(.upLeft, lineLength: times, move: newMove)
        if upLeftChips >= times { return true }

        let downRightChips = inLineChips(.downRight, lineLength: times - upLeftChips, move: newMove)
        if upLeftChips + downRightChips >= times { return true }

        return false
    }
}
