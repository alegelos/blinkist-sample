//
//  FileUtils.swift
//  Blinkist-Sample
//
//  Created by ___________ on 08/03/2020.
//  Copyright © 2020 idk. All rights reserved.
//

import Foundation

struct FileUtils {
    static func data(fromFile file: String, ext: String) -> Data {
        guard let url = Bundle.main.url(forResource: file, withExtension: ext),
            let data = try? Data(contentsOf: url) else {
                return Data()
        }
        return data
    }
}
