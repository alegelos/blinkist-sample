//
//  ConnectFourBoard+Additions.swift
//  Blinkist-Sample
//
//  Created by ___________ on 10/03/2020.
//  Copyright © 2020 idk. All rights reserved.
//

import Foundation
import ConnectFourGame

extension ConnectFour.ConnectFourBoard {

    /// Total slots in the board
    var slots: Int {
        guard let first = first else {
            return 0
        }
        return count * first.count
    }

    /// Returns the owner of the chip in the board from a collection item number
    /// - Parameter item: collection item number
    func owner(_ indexPath: IndexPath) -> Int? {
        let coordinates = indexPath.connectFourBoardCoordinates()

        return self[coordinates.x][coordinates.y]
    }
}
