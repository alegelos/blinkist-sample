//
//  Move+Additions.swift
//  Blinkist-Sample
//
//  Created by ___________ on 09/03/2020.
//  Copyright © 2020 idk. All rights reserved.
//

import Foundation
import ConnectFourGame

extension Move {
    /// Init a Move from a collection item number
    /// - Parameters:
    ///   - owner: player owner number
    ///   - collectionItem: collection item number
    init(owner: Int, indexPath: IndexPath) {
        let coords = indexPath.connectFourBoardCoordinates()

        self.init(x: coords.x, y: coords.y, owner: owner)
    }
}   
