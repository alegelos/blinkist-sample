//
//  ConnectFour+Additions.swift
//  Blinkist-Sample
//
//  Created by ___________ on 10/03/2020.
//  Copyright © 2020 idk. All rights reserved.
//

import Foundation
import ConnectFourGame

extension ConnectFour {
    /// Search for the next free slot to insert a chip in the ConnectFourGame board
    /// - Parameter indexPath: user touch indexpath
    func columnFreeSlot(at indexPath: IndexPath) -> IndexPath? {
        let coordinates = indexPath.connectFourBoardCoordinates()
        let column = board[coordinates.x]

        if column[coordinates.y] != nil {
            for newY in (0..<coordinates.y).reversed() {
                guard column[newY] == nil else {
                    continue
                }
                return IndexPath(boardX: coordinates.x, boardY: newY)
            }
        } else {
            var previousEmptyY = coordinates.y
            for newY in (coordinates.y..<ConnectFourConstants.MAX_Y) {
                guard column[newY] != nil else {
                    previousEmptyY = newY
                    continue
                }
                return IndexPath(boardX: coordinates.x, boardY: previousEmptyY)
            }
            return IndexPath(boardX: coordinates.x, boardY: previousEmptyY)
        }
        return nil
    }
}
