//
//  IndexPath+Additions.swift
//  Blinkist-Sample
//
//  Created by ___________ on 10/03/2020.
//  Copyright © 2020 idk. All rights reserved.
//

import Foundation

extension IndexPath {
    /// IndexPath from a ConnectFourGame board coordinates
    /// - Parameters:
    ///   - x: ConnectFourGame board x coordinate
    ///   - y: ConnectFourGame board y coordinate
    init(boardX x: Int, boardY y: Int) {
        let columnCount = y>0 ? y * ConnectFourConstants.MAX_X : 0
        self.init(item: columnCount + x, section: 0)
    }

    func connectFourBoardCoordinates() -> (x: Int, y: Int) {
        let doubleY = Double(item) / Double(ConnectFourConstants.MAX_X)
        let y = doubleY != 0 ? Int(doubleY) : 0
        var doubleX = (doubleY - Double(y)) * Double(ConnectFourConstants.MAX_X)
        doubleX.round()
        let x: Int = doubleX != 0 ? Int(doubleX) : 0
        return (x, y)
    }
}
