//
//  GameConfig.swift
//  Blinkist-Sample
//
//  Created by ___________ on 08/03/2020.
//  Copyright © 2020 idk. All rights reserved.
//

import Foundation

struct GameConfig: Codable {
    let id: Int
    let color1: String
    let color2: String
    let name1: String
    let name2: String
}
