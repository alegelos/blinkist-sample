//
//  GameServices.swift
//  Blinkist-Sample
//
//  Created by ___________ on 08/03/2020.
//  Copyright © 2020 idk. All rights reserved.
//

import Foundation
import Moya

enum GameServices {
    case config
}

// MARK: - TargetType
extension GameServices: TargetType {
    var baseURL: URL {
        guard let baseURL = URL(string: "https://private-75c7a5-blinkist.apiary-mock.com/connectFour/") else {
            fatalError("Failt to get GameService base URL")
        }
        return baseURL
    }

    var path: String {
        switch self {
        case .config:
            return "configuration"
        }
    }

    var method: Moya.Method {
        switch self {
        case .config:
            return .get
        }
    }

    var sampleData: Data {
        switch self {
        case .config:
            return FileUtils.data(fromFile: "Config", ext: "json")
        }
    }

    var task: Task {
        switch self {
        case .config:
            return .requestPlain
        }
    }

    var headers: [String : String]? {
        switch self {
        case .config:
            return ["Content-Type": "application/json"]
        }
    }
}
