//
//  BoardErrors.swift
//  Blinkist-Sample
//
//  Created by ___________ on 10/03/2020.
//  Copyright © 2020 idk. All rights reserved.
//

import Foundation

struct BoardErrors {
    enum Alerts: Error {
        case columnFull, failToStartGame
    }
}
