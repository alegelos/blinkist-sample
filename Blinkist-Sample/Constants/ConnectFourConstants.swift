//
//  ConnectFourConstants.swift
//  Blinkist-Sample
//
//  Created by ___________ on 08/03/2020.
//  Copyright © 2020 idk. All rights reserved.
//

import Foundation

struct ConnectFourConstants {
    /// Number of connection to win the game
    static let CONNECT_TIMES = 4

    /// Number of board colums
    static let MAX_X = 7

    /// Number of board row
    static let MAX_Y = 6
}

