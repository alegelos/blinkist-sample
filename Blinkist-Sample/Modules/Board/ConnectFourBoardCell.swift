//
//  ConnectFourBoardCell.swift
//  Blinkist-Sample
//
//  Created by ___________ on 10/03/2020.
//  Copyright © 2020 idk. All rights reserved.
//

import UIKit

class ConnectFourBoardCell: UICollectionViewCell {
    
    @IBOutlet weak var chipView: UIView!

    override func layoutSubviews() {
        super.layoutSubviews()

        chipView.layer.cornerRadius = chipView.bounds.width / 2
        chipView.layer.borderColor = UIColor.white.cgColor
        chipView.layer.borderWidth = 2
    }

    func setupCell(chipHexColor: String?) {
        guard let chipHexColor = chipHexColor,
            let chipColor = UIColor(hex: chipHexColor) else {
            chipView.backgroundColor = .clear
            return
        }
        chipView.backgroundColor = chipColor
    }
}
