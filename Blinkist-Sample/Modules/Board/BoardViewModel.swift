//
//  BoardViewModel.swift
//  Blinkist-Sample
//
//  Created by ___________ on 08/03/2020.
//  Copyright © 2020 idk. All rights reserved.
//

import Foundation
import ConnectFourGame
import Moya

protocol BoardViewModelDelegate: class {
    func setPlayersConfig(_ playersConfig: [Player])
    func reloadBoard(_ board: ConnectFour.ConnectFourBoard)
    func reloadBoardItems(_ board: ConnectFour.ConnectFourBoard, indexPaths: [IndexPath])
    func showAlert(message: BoardErrors.Alerts)
    func showWinner(_ player: Player)
    func showLoading()
    func hideLoading()
}

final class BoardViewModel {
    enum GameState {
        case starting, running, ended
    }

    weak var delegate: BoardViewModelDelegate?

    private let gameServices: MoyaProvider<GameServices>
    private var connectFour: ConnectFour
    private var players = [Player]()
    private var playerTurn: Int
    private var gameId: Int?
    private var gameState: GameState

    init(gameServices: MoyaProvider<GameServices> = MoyaProvider<GameServices>(),
         connectFour: ConnectFour = ConnectFour(connectTimes: ConnectFourConstants.CONNECT_TIMES,
                                                boardLenght: ConnectFourConstants.MAX_X,
                                                boardHeight: ConnectFourConstants.MAX_Y),
         delegate: BoardViewModelDelegate? = nil) {

        gameState = .starting
        self.gameServices = gameServices
        self.connectFour = connectFour
        playerTurn = 0
        self.delegate = delegate
        startGame()
    }

    func handleDidPress(_ indexPath: IndexPath) {
        guard gameState == .running else { return }
        guard let freeSlot = connectFour.columnFreeSlot(at: indexPath) else {
            delegate?.showAlert(message: .columnFull)
            return
        }
        let move = Move(owner: playerTurn, indexPath: freeSlot)

        connectFour.doMove(move)
        if connectFour.isWinnerMove(move) {
            gameState = .ended
            delegate?.showWinner(players[playerTurn])
        } else {
            endPlayerTurn()
        }
    }
}

// MARK: - ConnectFourDelegate
extension BoardViewModel: ConnectFourDelegate {
    func didChange(boardItems: [(x: Int, y: Int)]) {
        let indexPaths = boardItems.map { IndexPath(boardX: $0.x, boardY: $0.y) }
        delegate?.reloadBoardItems(connectFour.board, indexPaths: indexPaths)
    }

    func didChangeBoard() {
        delegate?.reloadBoard(connectFour.board)
    }
}

// MARK: - Private
extension BoardViewModel {
    private func startGame() {
        delegate?.showLoading()
        gameServices.request(.config) { [weak self] result in
            self?.delegate?.hideLoading()
            switch result {
            case .failure(let error):
                print(error)
                self?.delegate?.showAlert(message: .failToStartGame)
            case .success(let response):
                do {
                    let gameConfigs = try response.map([GameConfig].self)
                    guard let gameConfig = gameConfigs.first else {
                        throw GameServicesErrors.Response.emptyData
                    }
                    self?.loadGameConfig(gameConfig: gameConfig)
                    self?.startGameView()
                }
                catch {
                    print(error)
                    self?.delegate?.showAlert(message: .failToStartGame)
                }
            }
        }
    }

    private func loadGameConfig(gameConfig: GameConfig) {
        let playerOne = Player(name: gameConfig.name1,
                               color: gameConfig.color1)
        let playerTwo = Player(name: gameConfig.name2,
                               color: gameConfig.color2)
        players = [playerOne, playerTwo]
        gameId = gameConfig.id
    }

    private func startGameView() {
        gameState = .running
        connectFour.delegate = self
        delegate?.setPlayersConfig(players)
        delegate?.reloadBoard(connectFour.board)
    }

    private func endPlayerTurn() {
        guard playerTurn < players.count - 1 else {
            playerTurn = 0
            return
        }
        playerTurn += 1
    }
}
