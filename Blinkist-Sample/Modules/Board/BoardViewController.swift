//
//  BoardViewController.swift
//  Blinkist-Sample
//
//  Created by ___________ on 08/03/2020.
//  Copyright © 2020 idk. All rights reserved.
//

import UIKit
import ConnectFourGame

final class BoardViewController: UIViewController {

    @IBOutlet weak var boardCollectionView: UICollectionView!

    private let boardViewModel = BoardViewModel()
    private let boardCollectionHandler = BoardCollectionHandler()

    override func viewDidLoad() {
        super.viewDidLoad()

        setupBoardCollection()
        setupBoardViewModel()
    }

}

// MARK: - BoardCollectionHandlerDelegate
extension BoardViewController: BoardCollectionHandlerDelegate {
    func didPress(_ indexPath: IndexPath) {
        boardViewModel.handleDidPress(indexPath)
    }
}

// MARK: - BoardViewModelDelegate
extension BoardViewController: BoardViewModelDelegate {
    func setPlayersConfig(_ playersConfig: [Player]) {
        boardCollectionHandler.players = playersConfig
    }

    func showWinner(_ player: Player) {
        print(player.name + "won the game")
        // TODO
    }

    func showLoading() {
        print("Start loding")
        // TODO
    }

    func hideLoading() {
        print("Stop loading")
        // TODO
    }

    func showAlert(message: BoardErrors.Alerts) {
        print("show alert" + message.localizedDescription)
        // TODO
    }

    func reloadBoardItems(_ board: ConnectFour.ConnectFourBoard,
                          indexPaths: [IndexPath]) {
        boardCollectionHandler.board = board
        boardCollectionView.reloadItems(at: indexPaths)
    }

    func reloadBoard(_ board: ConnectFour.ConnectFourBoard) {
        boardCollectionHandler.board = board
        boardCollectionView.reloadData()
    }
}

// MARK: - Private
extension BoardViewController {
    private func setupBoardCollection() {
        boardCollectionView.delegate = boardCollectionHandler
        boardCollectionView.dataSource = boardCollectionHandler
        boardCollectionHandler.delegate = self
    }

    private func setupBoardViewModel() {
        boardViewModel.delegate = self
    }
}
