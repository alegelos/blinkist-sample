//
//  BoardCollectionHandler.swift
//  Blinkist-Sample
//
//  Created by ___________ on 08/03/2020.
//  Copyright © 2020 idk. All rights reserved.
//

import UIKit
import ConnectFourGame

protocol BoardCollectionHandlerDelegate: class {
    func didPress(_ indexPath: IndexPath)
}

final class BoardCollectionHandler: NSObject {
    private static let BOARD_CELL = "ConnectFourCell"

    weak var delegate: BoardCollectionHandlerDelegate?

    var board = ConnectFour.ConnectFourBoard()
    var players = [Player]()
}

// MARK: - UICollectionViewDelegate
extension BoardCollectionHandler: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.didPress(indexPath)
    }
}

extension BoardCollectionHandler: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        let numberofItem: CGFloat = CGFloat(ConnectFourConstants.MAX_X)
        let collectionViewWidth = collectionView.bounds.width
        let extraSpace = (numberofItem - 1) * flowLayout.minimumInteritemSpacing
        let horizaontalInset = flowLayout.sectionInset.right + flowLayout.sectionInset.left
        let width = Int((collectionViewWidth - extraSpace - horizaontalInset) / numberofItem)

        return CGSize(width: width, height: width)
    }
}

// MARK: - UICollectionViewDataSource
extension BoardCollectionHandler: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return board.slots
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: BoardCollectionHandler.BOARD_CELL,
                                                            for: indexPath) as? ConnectFourBoardCell else {
                                                                return UICollectionViewCell()
        }
        var chipColor: String?
        if let owner = board.owner(indexPath) {
            chipColor = players[owner].color
        }
        cell.setupCell(chipHexColor: chipColor)
        return cell
    }
}
